package pl.sdacademy.wurst.recyclerview;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import pl.sdacademy.wurst.R;
import pl.sdacademy.wurst.db.DaoMaster;
import pl.sdacademy.wurst.db.DaoSession;
import pl.sdacademy.wurst.db.Order;
import pl.sdacademy.wurst.db.OrderDao;
import pl.sdacademy.wurst.db.Person;
import pl.sdacademy.wurst.db.PersonDao;

/**
 * Created by RENT on 2017-07-11.
 */

public class WurstAdapter extends RecyclerView.Adapter<WurstAdapter.MyViewHolder> {

    private List<Person> persons;
    private Context context;

    public WurstAdapter(List<Person> persons, Context context) {
        this.persons = persons;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_row, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Person person = persons.get(position);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "users.db");
        Database db = helper.getWritableDb();
        DaoSession daoSession = new DaoMaster(db).newSession();
        final PersonDao personDao = daoSession.getPersonDao();
        final OrderDao orderDao = daoSession.getOrderDao();

        List<Order> orderList = orderDao.queryBuilder().where(OrderDao.Properties.PersonId.eq(person.getId())).list();
        String orderToSet = "";
        for (Order order : orderList) {
            orderToSet = orderToSet + order.getProduct() + " : " + order.getQuantity() + "\n";
        }

        holder.textViewPersonName.setText(person.getName());
        holder.textViewPersonPrepay.setText("zal: " + String.valueOf(person.getPrepay()));
        holder.textViewPersonOrder.setText(orderToSet);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                final View view = View.inflate(context, R.layout.recyclerview_onclick, null);
                alertDialog.setView(view);


                final EditText editTextRecyclerPersonName = (EditText) view.findViewById(R.id.editTextRecyclerPersonName);
                final EditText editTextRecyclerPersonPrepay = (EditText) view.findViewById(R.id.editTextRecyclerPersonPrepay);
                final EditText editTextRecyclerPersonOrderSplit1 = (EditText) view.findViewById(R.id.editTextRecyclerPersonOrderSplit1);
                final EditText editTextRecyclerPersonOrderSplit2 = (EditText) view.findViewById(R.id.editTextRecyclerPersonOrderSplit2);
                final EditText editTextRecyclerPersonOrderSplit3 = (EditText) view.findViewById(R.id.editTextRecyclerPersonOrderSplit3);
                final EditText editTextRecyclerPersonOrderSplit4 = (EditText) view.findViewById(R.id.editTextRecyclerPersonOrderSplit4);
                final EditText editTextRecyclerPersonOrderSplit5 = (EditText) view.findViewById(R.id.editTextRecyclerPersonOrderSplit5);
                final EditText editTextRecyclerPersonOrderSplit6 = (EditText) view.findViewById(R.id.editTextRecyclerPersonOrderSplit6);
                final EditText[] editTexts = {editTextRecyclerPersonOrderSplit1, editTextRecyclerPersonOrderSplit2, editTextRecyclerPersonOrderSplit3,
                                                editTextRecyclerPersonOrderSplit4, editTextRecyclerPersonOrderSplit5, editTextRecyclerPersonOrderSplit6};

                editTextRecyclerPersonName.setText(person.getName());
                editTextRecyclerPersonPrepay.setText(String.valueOf(person.getPrepay()));
                // dodać wyświetlanie w editTexie po clicku na recyclerView
//                String[] orderList = person.getOrder().split("\n");
//                for (int i = 0; i < orderList.length; i++) {
//                    editTexts[i].setText(orderList[i]);
//                }

                alertDialog.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Person personDoAktualizacj = person;

                        personDoAktualizacj.setName(editTextRecyclerPersonName.getText().toString());
                        personDoAktualizacj.setPrepay(Float.valueOf(editTextRecyclerPersonPrepay.getText().toString()));
                        // dodać aktualizację zamówieia


                        personDao.update(personDoAktualizacj);

                        notifyDataSetChanged();


                    }
                }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {

        return persons.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewPersonName;
        TextView textViewPersonPrepay;
        TextView textViewPersonOrder;

        public MyViewHolder(View itemView) {
            super(itemView);
            textViewPersonName = (TextView) itemView.findViewById(R.id.textViewPersonName);
            textViewPersonPrepay = (TextView) itemView.findViewById(R.id.textViewPersonPrepay);
            textViewPersonOrder = (TextView) itemView.findViewById(R.id.textViewPersonOrder);
        }
    }
}
