package pl.sdacademy.wurst;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import pl.sdacademy.wurst.db.DaoMaster;
import pl.sdacademy.wurst.db.DaoSession;
import pl.sdacademy.wurst.db.Order;
import pl.sdacademy.wurst.db.OrderDao;
import pl.sdacademy.wurst.db.Person;
import pl.sdacademy.wurst.db.PersonDao;
import pl.sdacademy.wurst.recyclerview.WurstAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "log";
    private Context context;

    private DaoSession daoSession;
    private PersonDao personDao;
    private OrderDao orderDao;
    private List<Person> persons;

    private RecyclerView recyclerView;
    private LinearLayoutManager llm;
    private WurstAdapter wurstAdapter;


    private Button addPersonButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;

        DaoMaster.DevOpenHelper helperDB = new DaoMaster.DevOpenHelper(context, "users.db");
        Database db = helperDB.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        personDao = daoSession.getPersonDao();
        orderDao = daoSession.getOrderDao();
        persons = personDao.queryBuilder().list();

        recyclerView = (RecyclerView) findViewById(R.id.myRecycleView);
        llm = new LinearLayoutManager(context);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), llm.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(llm);
        wurstAdapter = new WurstAdapter(persons, context);
        recyclerView.setAdapter(wurstAdapter);

        addPersonButton = (Button) findViewById(R.id.addPersonButton);
        addPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPerson(context);
            }
        });

    }

    public void addPerson(final Context context) {
        Log.d(TAG, "in addPerson method");


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        final View view = View.inflate(context, R.layout.add_person_layout, null);
        alertDialog.setView(view);
        alertDialog.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "in onClick dialogAllert addPerson method");

                EditText editTextPersonNameAlertDialogAdd = (EditText) view.findViewById(R.id.editTextPersonNameAlertDialogAdd);
                EditText editTextPersonPrepayDialogAdd = (EditText) view.findViewById(R.id.editTextPersonPrepayDialogAdd);
                Spinner spinner1 = (Spinner) view.findViewById(R.id.spinner1);
                EditText editTextSpinner1 = (EditText) view.findViewById(R.id.editTextSpinner1);
                Spinner spinner2 = (Spinner) view.findViewById(R.id.spinner2);
                EditText editTextSpinner2 = (EditText) view.findViewById(R.id.editTextSpinner2);
                Spinner spinner3 = (Spinner) view.findViewById(R.id.spinner3);
                EditText editTextSpinner3 = (EditText) view.findViewById(R.id.editTextSpinner3);
                Spinner spinner4 = (Spinner) view.findViewById(R.id.spinner4);
                EditText editTextSpinner4 = (EditText) view.findViewById(R.id.editTextSpinner4);
                Spinner spinner5 = (Spinner) view.findViewById(R.id.spinner5);
                EditText editTextSpinner5 = (EditText) view.findViewById(R.id.editTextSpinner5);
                Spinner spinner6 = (Spinner) view.findViewById(R.id.spinner6);
                EditText editTextSpinner6 = (EditText) view.findViewById(R.id.editTextSpinner6);

                Spinner[] spinners = {spinner1, spinner2, spinner3, spinner4, spinner5, spinner6};
                EditText[] editTexts = {editTextSpinner1, editTextSpinner2, editTextSpinner3, editTextSpinner4, editTextSpinner5, editTextSpinner6};

                Person person = new Person();
                person.setName(editTextPersonNameAlertDialogAdd.getText().toString());
                person.setPrepay(Float.valueOf(editTextPersonPrepayDialogAdd.getText().toString()));
                long personId = personDao.insert(person);

                for(int i = 0; i < 6; i++){
                    Order order = new Order();
                    if(!spinners[i].getSelectedItem().toString().equals("Puste")){
                    order.setPersonId(personId);
                    order.setProduct(spinners[i].getSelectedItem().toString());
                    order.setQuantity(Float.valueOf(editTexts[i].getText().toString()));
                    }
                    orderDao.insert(order);
                }

                persons = personDao.queryBuilder().list();
                recyclerView.setAdapter(new WurstAdapter(persons, context));

            }
        }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
