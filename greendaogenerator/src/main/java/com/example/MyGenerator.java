package com.example;


import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;

public class MyGenerator {

    public static void main(String[] args) {

        Schema schema = new Schema(3, "pl.sdacademy.wurst.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);


        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables (Schema schema){
        Entity person = schema.addEntity("Person");
        person.addIdProperty().primaryKey().autoincrement();
        person.addStringProperty("name");
        person.addFloatProperty("prepay");

        Entity order = schema.addEntity("Order");
        order.addIdProperty().primaryKey().autoincrement();
        order.addStringProperty("product");
        order.addFloatProperty("quantity");
        Property personIdProperty = order.addLongProperty("personId").getProperty();
        order.addToOne(person, personIdProperty);


    }

}
